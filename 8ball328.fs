@include doloop.fs
\ *********************************************************************
\                                                                     *
\    Filename:      8ball.fs                                          *
\    Date:          2021-05-07                                        *
\    Author:        Dave Bucklin                                      *
\ *********************************************************************
\ Magic 8-Ball!
\ *********************************************************************
\
\ special function registers
decimal
$0023 constant pinb
$0024 constant ddrb
$0025 constant portb
$0026 constant pinc
$0027 constant ddrc
$0028 constant portc

\ bitmasks
\ # denotes a literal decimal integer
1 #7 lshift constant bit7
1 #6 lshift constant bit6
1 #5 lshift constant bit5
1 #4 lshift constant bit4
1 #3 lshift constant bit3
1 #2 lshift constant bit2
1 #1 lshift constant bit1
1 constant bit0

\ pin definitions
: lcd-e    bit0 portc ;
: lcd-rw   bit1 portc ;
: lcd-db4  bit1 portb ;
: lcd-db5  bit2 portb ;
: lcd-db6  bit3 portb ;
: lcd-db7  bit4 portb ;
: lcd-rs   bit5 portb ;
: button   bit2 portc ;

\ pin actions
: output ( bit port -- ) 1 - mset ;
: input ( bit port -- ) 1 - mclr ;
: high ( bit port -- ) mset ;
: low ( bit port -- ) mclr ;
: ?button ( -- f ) pinc @ bit2 and ;

: reset-pins
  lcd-e   low
  lcd-rw  low
  lcd-db4 low
  lcd-db5 low
  lcd-db6 low
  lcd-db7 low
  lcd-rs  low
  button  low ;

: init-pins
  lcd-e   output
  lcd-rw  output
  lcd-db4 output
  lcd-db5 output
  lcd-db6 output
  lcd-db7 output
  lcd-rs  output
  button  input 
  reset-pins ;

-lcd
marker -lcd
\ lcd commands
: clock     2 ms lcd-e high 2 ms lcd-e low ;
: write     lcd-rw low ;
: read      lcd-rw high ;
: data      lcd-rs high ;
: inst      lcd-rs low ;

: nibble ( byte -- )
  \ we only care about the low-order bits, so mask off the high ones
  %00001111 and
  \ lshift once to align with portb field
  1 lshift
  \ apply mask to portb to set bits 1-4 to zero, and
  portb @ %11100001 and
  \ add our nibble to portb and store
  + portb ! ;

: lcd-send ( byte -- )
    dup 4 rshift nibble clock nibble clock
;

: lcd-init
  inst
  #03 nibble clock
  #03 nibble clock
  #03 nibble clock
  #02 nibble clock
  #02 nibble clock
  #12 nibble clock
  #08 lcd-send
  #01 lcd-send
  #06 lcd-send
  #12 lcd-send
  data
;

: init      init-pins lcd-init ;
: >line1    inst $00 $80 or lcd-send ;
: >line2    inst $40 $80 or lcd-send ;
: lcd-clear inst 1 lcd-send ;
: lcd-home  inst 2 lcd-send ;
: right     inst 0 do 28 lcd-send loop ;
: left      inst 0 do 24 lcd-send loop ;
: lcd-type ( addr len -- )
    data 0 do dup i + @ lcd-send loop drop
;

-game
marker -game
32 constant #buf
8 constant #queries
8 constant #answers

flash
create greeting
," Ready for your  query... (^   ^)"

create queries
," Checking behind the fridge...   "
," Summoning       Azrael...       "
," Consulting      the stars...    "
," Calculating...  beep...  boop..."
," Consulting the  farm journals..."
," Googling        Wikipedia...    "
," Hacking N2 yr   bank accountz..."
," Checking your   browser history "
," Let me think    about that...   "
," Lets think aboutthis a moment..."

create answers
," Signs point to  Yes.            "
," Check next to   your keys.      "
," Check your otherpants.          "
," It is simply    unknowable.     "
," A FOIA request  is required.    "
," The final answeris...     Maybe?"
," Possible, but   inadvisable.    "
," You don't wanna know.           "
," Let me call you back later.     "
," You'll have to  ask Jane.       "
," Ask Jane. I'll  bet she knows.  "
ram

\ put two lines on the lcd
: show ( addr -- )
    lcd-home dup >line1 16 lcd-type
    >line2 16 + 16 lcd-type
;

\ get a random-ish number
: badrnd ( -- n ) ticks 7 and ;

\ gimme has to add badrnd 2 * 1 + because, in FF, ," creates a counted string
\ this means there's an extra byte in front of each string that contains
\ the number of bytes in the string.
: gimme ( addr -- addr )
    badrnd dup 2 * 1 + swap #buf * + +
;

\ make a cute little face
: lefteye   inst $4b $80 or lcd-send ;
: righteye  inst $4d $80 or lcd-send ;
: nose      inst $4c $80 or lcd-send ;
: opened    data $a5 lcd-send ;
: closed    data $2d lcd-send ;
: cute      data $f3 lcd-send ;
: eyesopen  righteye opened lefteye opened ;
: eyesclosed righteye closed lefteye closed ;
: emoji     eyesopen nose cute ;
: blink     250 ms eyesclosed 250 ms eyesopen ;

\ main words
: ready     greeting 1 + show nose cute blink reset-pins ;
: q&a       queries gimme show 3997 ms answers gimme show 3997 ms ;
: 8ball     init ready begin ?button if q&a ready then 197 ms key? until ;
