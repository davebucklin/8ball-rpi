# Magic 8-Ball

Dependencies: 

* A Raspberry Pi
* [gforth](https://gforth.org)
* [wiringPi for gforth](https://github.com/kristopherjohnson/wiringPi_gforth)
  * https://www.raspberrypi.org/forums/viewtopic.php?t=207597
  * http://http://wiringpi.com
* A 16x2 LCD
  * A TC1602A-01T in this case, using an HD44780
  * I'm running it in 4-bit mode
* A bunch of jumpers and stuff
  * I might create a diagram someday

This is really just a prototyping effort.
My goal is to implement this using [FlashForth](https://www.flashforth.com) on an Atmel Atmega328P
and run it off batteries.
