\ empty
include lcd.fs

\ Reserve some memory for all the strings
32 constant #buf
8 constant #queries
8 constant #answers
create greeting #buf allot
create queries #buf #queries * allot
queries #buf #queries * 32 fill \ spaces
create answers #buf #answers * allot
answers #buf #answers * 32 fill \ spaces

: stor ( addr len pos addr -- ) swap #buf * + swap move ;

s" Ready for your  query... (^   ^)" 0 greeting stor
s" Checking behind the fridge..." 0 queries stor
s" Summoning       Azrael..." 1 queries stor
s" Consulting      the stars..." 2 queries stor
s" Calculating...  beep...  boop..." 3 queries stor
s" Consulting the  farm journals..." 4 queries stor
s" Googling        Wikipedia..." 5 queries stor
s" Hacking N2 yr   bank accountz..." 6 queries stor
s" Checking your   browser history" 7 queries stor

s" Signs point to  Yes." 0 answers stor
s" Check next to   your keys." 1 answers stor
s" Check your otherpants." 2 answers stor
s" It is simply    unknowable." 3 answers stor
s" A FOIA request  is required." 4 answers stor
s" The final answeris...     Maybe?" 5 answers stor
s" Possible, but   inadvisable." 6 answers stor
s" You don't wanna know." 7 answers stor

: show ( addr -- ) lcd-home dup >line1 16 lcd-type >line2 16 + 16 lcd-type ;
: badrnd utime drop 7 and ;
: gimme ( addr -- addr ) badrnd #buf * + ;
: lefteye inst $4b $80 or lcd-send ;
: righteye inst $4d $80 or lcd-send ;
: nose inst $4c $80 or lcd-send ;
: opened data $a5 lcd-send ;
: closed data $2d lcd-send ;
: cute data $f3 lcd-send ;
: eyesopen righteye opened lefteye opened ;
: eyesclosed righteye closed lefteye closed ;
: emoji eyesopen nose cute ;
: blink 250 ms eyesclosed 250 ms eyesopen ;
: ready greeting show nose cute blink reset-pins ;
: q&a queries gimme show 4000 ms answers gimme show 4000 ms ;
: refresh page s" Magic 8-Box is here to answer your questions" type cr s" State your question and press ENTER" type cr cr ;
: 8ball ready begin 21 digitalread 0 = if q&a ready blink then 200 ms again ;
