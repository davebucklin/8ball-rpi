\ utilities for 16x2 HD44780U LCD Display
\ running in 4-bit mode

include wiringPi.fs

\ pin map to Pi Zero
4 constant lcd-e     \ enable 
22 constant lcd-rw   \ RW 1:read/0:write
5 constant lcd-db4   \ DB4
6 constant lcd-db5   \ DB5
13 constant lcd-db6  \ DB6
19 constant lcd-db7  \ DB7
26 constant lcd-rs   \ RS 1:data/0:inst
21 constant button   \ input button

: up high digitalwrite ;
: down low digitalwrite ;
: clock lcd-e up 1 ms lcd-e down 1 ms ;
: write lcd-rw down ;
: read lcd-rw up ;
: data lcd-rs up ;
: inst lcd-rs down ;

: reset-pins
  lcd-e down  
  lcd-rw down 
  lcd-db4 down
  lcd-db5 down
  lcd-db6 down
  lcd-db7 down
  lcd-rs down
  button up ;

: init-pins
  lcd-e output pinmode  
  lcd-rw output pinmode 
  lcd-db4 output pinmode
  lcd-db5 output pinmode
  lcd-db6 output pinmode
  lcd-db7 output pinmode
  lcd-rs output pinmode 
  button output pinmode
  reset-pins ;

: nibble ( byte -- )
  dup 1 and lcd-db4 swap digitalwrite
  dup 2 and 1 rshift lcd-db5 swap digitalwrite
  dup 4 and 2 rshift lcd-db6 swap digitalwrite
      8 and 3 rshift lcd-db7 swap digitalwrite ;

: lcd-send ( byte -- ) dup 4 rshift nibble clock nibble clock ;
: lcd-init
  inst
  03 nibble clock
  03 nibble clock
  03 nibble clock
  02 nibble clock
  02 nibble clock
  12 nibble clock
  08 lcd-send
  01 lcd-send
  06 lcd-send
  12 lcd-send
  data ;

: init wiringpisetupgpio throw init-pins lcd-init ;
: >line1 inst $00 $80 or lcd-send ;
: >line2 inst $40 $80 or lcd-send ;
: lcd-type ( addr len -- ) data 0 do dup i + @ lcd-send loop ;
: lcd-clear inst 1 lcd-send ;
: lcd-home inst 2 lcd-send ;
: right inst 0 do 28 lcd-send loop ;
: left inst 0 do 24 lcd-send loop ;

